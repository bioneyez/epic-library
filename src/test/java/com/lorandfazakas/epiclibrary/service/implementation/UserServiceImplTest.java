package com.lorandfazakas.epiclibrary.service.implementation;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.repository.RoleRepository;
import com.lorandfazakas.epiclibrary.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setEmail("test@test.com");
        user.setName("test test");
        user.setPassword("test");
        user.setUsername("test");
        user.setEnabled(true);
        user.setBarcode("0000000");
        user.setZipCode("0000");
        user.setCity("City name");
        user.setStreetAddress(" Test address 31");
    }

    @Test
    public void saveUser_callUserRepositorySaveMethod() throws Exception {
        userService.save(user);
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void deleteUser_callUserRepositoryDeleteMethod() {
        userService.delete(user);
        verify(userRepository, times(1)).delete(user);
    }

    @Test
    public void registerUser_callUserRepositorySaveMethod() {
        userService.register(user);
        assert(user.isEnabled());
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void findUserByUsername_callUserRepositoryFindByUsernameMethod() {
        userService.findByUsername(anyString());
        verify(userRepository, times(1)).findByUsername(anyString());
    }

    @Test
    public void findUserByEmail_callUserRepositoryFindByEmailMethod() {
        userService.findByEmail(anyString());
        verify(userRepository, times(1)).findByEmail(anyString());
    }

    @Test
    public void findUserById_callUserRepositoryFindOneMethod() {
        userService.findById(anyLong());
        verify(userRepository, times(1)).findOne(anyLong());
    }

    @Test
    public void findUserByBarcode_callUserRepositoryFindByBarcodeMethod() {
        userService.findByBarcode(anyString());
        verify(userRepository, times(1)).findByBarcode(anyString());
    }

    @Test
    public void findUserByNameStartingWith_callUserRepositoryFindByNameStartingWithMethod() {
        userService.findByNameStartingWith(anyString());
        verify(userRepository, times(1)).findByNameStartingWith(anyString());
    }

    @Test
    public void findUserByNameStartingWithOrBarcode_callUserRepositoryFindByNameStartingWithOrBarcodeMethod() {
        userService.findByNameStartingWithOrBarcode("111111");
        verify(userRepository, times(1)).findByBarcode("111111");

        userService.findByNameStartingWithOrBarcode("Test");
        verify(userRepository, times(1)).findByNameStartingWith("Test");
    }
}