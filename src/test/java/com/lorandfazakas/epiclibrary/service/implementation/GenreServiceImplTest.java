package com.lorandfazakas.epiclibrary.service.implementation;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.repository.GenreRepository;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class GenreServiceImplTest {

    @Mock
    private GenreRepository genreRepository;

    @InjectMocks
    private GenreServiceImpl genreService;

    private Genre genre;

    @Before
    public void setUp() throws Exception {
        genre = new Genre("Test" );
    }


    @Test
    public void findGenreById_callGenreRepositoryFindOneMethod() {
        genreService.findById(anyLong());
        verify(genreRepository, times(1)).findOne(anyLong());

    }

    @Test
    public void findGenreByName_callGenreRepositoryFindByNameMethod() {
        genreService.findByName(anyString());
        verify(genreRepository, times(1)).findByName(anyString());
    }

    @Test
    public void findGenreByNameContains_callGenreRepositoryFindByNameContainsMethod() {
        genreService.findByNameContains(anyString());
        verify(genreRepository, times(1)).findByNameContains(anyString());
    }


}