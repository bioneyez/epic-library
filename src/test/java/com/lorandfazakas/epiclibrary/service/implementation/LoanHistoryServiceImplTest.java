package com.lorandfazakas.epiclibrary.service.implementation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Loan;
import com.lorandfazakas.epiclibrary.entity.LoanHistory;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.repository.LoanHistoryRepository;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class LoanHistoryServiceImplTest {

    @Mock
    private LoanHistoryRepository loanHistoryRepository;

    @InjectMocks
    private LoanHistoryServiceImpl loanHistoryService;

    private LoanHistory loanHistory;

    @Before
    public void setUp() throws Exception {
        User user = new User();
        Book book = new Book("11111", true);
        Loan loan = new Loan(user,book);
        loanHistory = new LoanHistory(loan);
    }

    @Test
    public void saveLoanHistory_callLoanHistoryRepositorySaveMethod() throws Exception {
        loanHistoryService.save(loanHistory);
        verify(loanHistoryRepository,times(1)).save(loanHistory);
    }

    @Test
    public void deleteLoanHistory_callLoanHistoryRepositoryDeleteMethod() {
        loanHistoryService.delete(loanHistory);
        verify(loanHistoryRepository, times(1)).delete(loanHistory);
    }

    @Test
    public void findLoanHistoryById_callLoanHistoryRepositoryFindOneMethod() {
        loanHistoryService.findById(anyLong());
        verify(loanHistoryRepository, times(1)).findOne(anyLong());
    }



}