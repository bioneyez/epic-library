package com.lorandfazakas.epiclibrary.service.implementation;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.lorandfazakas.epiclibrary.service.BookService;
import com.lorandfazakas.epiclibrary.service.LoanHistoryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.configuration.LibraryConfiguration;
import com.lorandfazakas.epiclibrary.exception.BookNotAvailableException;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Loan;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.repository.BookRepository;
import com.lorandfazakas.epiclibrary.repository.LoanHistoryRepository;
import com.lorandfazakas.epiclibrary.repository.LoanRepository;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class LoanServiceImplTest {
    private static final long BOOK_LOAN_PERIOD_IN_DAY = LibraryConfiguration.getBookLoanPeriodInDay();
    private static final long OVERDUE_FEE_PER_DAY = LibraryConfiguration.getOverdueFeePerDay();

    @Mock
    private LoanRepository loanRepository;

    @Mock
    private LoanHistoryService loanHistoryService;

    @Mock
    private BookService bookService;

    @InjectMocks
    private LoanServiceImpl loanService;

    private User user;

    private Book book;

    private Loan loan;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setEmail("test@test.com");
        user.setName("test test");
        user.setPassword("test");
        user.setUsername("test");
        user.setEnabled(true);
        user.setBarcode("0000000");
        user.setZipCode("0000");
        user.setCity("City name");
        user.setStreetAddress(" Test address 31");

        book = new Book("testBarcode",true);

        loan = new Loan(user,book);

    }

    @Test
    public void saveLoan_callLoanRepositorySaveMethod() throws Exception {
        loanService.save(loan);
        verify(loanRepository, times(1)).save(loan);
    }


    @Test
    public void deleteLoan_callLoanRepositoryDeleteMethod() {
        loanService.delete(loan);
        verify(loanRepository, times(1)).delete(loan);
    }

    @Test
    public void findLoanById_callLoanRepositoryFindOneMethod() {
        loanService.findById(anyLong());
        verify(loanRepository, times(1)).findOne(anyLong());
    }

    @Test
    public void findLoanByUser_callLoanRepositoryFindByUserMethod() {
        loanService.findByUser(user);
        verify(loanRepository, times(1)).findByUser(user);
    }

    @Test
    public void loanAvailableBook_SetBookAvailabilityToFalseAndAddLoanToUserLoans() throws BookNotAvailableException{
        loanService.loanBook(user,book);
        List<Loan> userLoansList = new ArrayList<>(user.getLoans());
        Book loanedBookByUser = userLoansList.get(0).getBook();
        assertEquals(false,book.isAvailable());
        assertEquals(book, loanedBookByUser);
    }

    @Test(expected = BookNotAvailableException.class)
    public void loanNotAvailableBook() throws BookNotAvailableException {
        book.setAvailable(false);
        loanService.loanBook(user,book);
    }

    @Test
    public void returnBook_SetBookAvailabilityToTrueAndRemoveLoanFromUserLoansAndArchiveLoan() throws Exception{
        book.setAvailable(false);
        loan = new Loan(user,book);
        when(loanRepository.findByBook(book)).thenReturn(loan);
        loanService.returnBook(book);
        assertEquals(true,book.isAvailable());
        assertTrue(user.getLoans().isEmpty());
    }

    @Test
    public void calculateOverdueFeeForReturnWithin2Days() throws Exception {
        long dayCount = 2;
        long overdueDayCount = calculateOverdueDayCount(dayCount);
        Loan loan = new Loan(user,book);
        loan.setLoanDate(LocalDate.now().minusDays(dayCount));
        assertEquals(overdueDayCount*OVERDUE_FEE_PER_DAY,loanService.calculateOverdueFee(loan));
    }

    @Test
    public void calculateOverdueFeeForReturnIn91Days() throws Exception {
        int dayCount = 91;
        long overdueDayCount = calculateOverdueDayCount(dayCount);
        Loan loan = new Loan(user,book);
        loan.setLoanDate(LocalDate.now().minusDays(dayCount));
        loan.setReturnDateDeadline(loan.getLoanDate().plusDays(LibraryConfiguration.getBookLoanPeriodInDay()));
        assertEquals(overdueDayCount*OVERDUE_FEE_PER_DAY,loanService.calculateOverdueFee(loan));
    }

    @Test
    public void calculateOverdueFeeForReturnIn100Days() throws Exception {
        int dayCount = 100;
        long overdueDayCount = calculateOverdueDayCount(dayCount);
        Loan loan = new Loan(user,book);
        loan.setLoanDate(LocalDate.now().minusDays(dayCount));
        loan.setReturnDateDeadline(loan.getLoanDate().plusDays(LibraryConfiguration.getBookLoanPeriodInDay()));
        assertEquals(overdueDayCount*OVERDUE_FEE_PER_DAY,loanService.calculateOverdueFee(loan));
    }

    private long calculateOverdueDayCount(long dayCount) {
        long overdueDayCount = 0;
        if (dayCount > BOOK_LOAN_PERIOD_IN_DAY) {
            overdueDayCount = dayCount - BOOK_LOAN_PERIOD_IN_DAY;
        }
        return overdueDayCount;
    }



}