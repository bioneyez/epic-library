package com.lorandfazakas.epiclibrary.service.implementation;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.repository.BibliographicRecordRepository;

import java.util.HashSet;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class BibliographicRecordServiceImplTest {
    @Mock
    private BibliographicRecordRepository bibliographicRecordRepository;

    @InjectMocks
    private BibliographicRecordServiceImpl bibliographicRecordService;

    private BibliographicRecord bibliographicRecord;

    @Before
    public void setUp() throws Exception {
        bibliographicRecord = new BibliographicRecord("testTitle", "testAuthor", "978-3-16-148410-0", new Genre("Drama"));
        bibliographicRecord.setId(1L);
    }

    @Test
    public void saveBibliographicRecord_callBibliographicRecordRepositorySaveMethod() throws Exception {
        bibliographicRecordService.save(bibliographicRecord);
        verify(bibliographicRecordRepository, times(1)).save(bibliographicRecord);
    }

    @Test
    public void deleteBibliographicRecord_callBibliographicRecordRepositoryDeleteMethod() {
        bibliographicRecordService.delete(bibliographicRecord);
        verify(bibliographicRecordRepository, times(1)).delete(bibliographicRecord);
    }

    @Test
    public void findBibliographicRecordById_callBibliographicRecordRepositoryFindOneMethod() {
        when(bibliographicRecordRepository.findOne(bibliographicRecord.getId())).thenReturn(bibliographicRecord);
        assertEquals(bibliographicRecordService.findById(bibliographicRecord.getId()),bibliographicRecord);
        verify(bibliographicRecordRepository, times(1)).findOne(bibliographicRecord.getId());
    }


    @Test
    public void findBibliographicRecordByIsbn_callBibliographicRecordRepositoryFindByIsbnMethod() {
        when(bibliographicRecordRepository.findByIsbn(bibliographicRecord.getIsbn())).thenReturn(bibliographicRecord);
        assertEquals (bibliographicRecordService.findByIsbn(bibliographicRecord.getIsbn()),bibliographicRecord);
        verify(bibliographicRecordRepository, times(1)).findByIsbn(bibliographicRecord.getIsbn());
    }

    @Test
    public void findBibliographicRecordByIsbnStartingWith() {
        HashSet<BibliographicRecord> bibliographicRecordHashSet = new HashSet<>();
        bibliographicRecordHashSet.add(bibliographicRecord);
        when(bibliographicRecordRepository.findByIsbnStartingWith(bibliographicRecord.getIsbn())).thenReturn(bibliographicRecordHashSet);
        bibliographicRecordService.findByIsbnStartingWith(bibliographicRecord.getIsbn());
        verify(bibliographicRecordRepository, times(1)).findByIsbnStartingWith(bibliographicRecord.getIsbn());
    }


    @Test
    public void findPageableBibliographicRecordByTitleContainingOrAuthorContainingOrderByTitle() {
        bibliographicRecordService.findPageableByTitleContainingOrAuthorContainingOrderByTitle(anyString(),anyString(),any());
        verify(bibliographicRecordRepository, times(1))
                .findByTitleContainingOrAuthorContainingOrderByTitle(anyString(), anyString(), any());
    }



}