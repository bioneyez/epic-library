package com.lorandfazakas.epiclibrary.service.implementation;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.repository.BookRepository;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class BookServiceImplTest {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookServiceImpl bookService;

    private Book book;

    @Before
    public void setUp() throws Exception {
        book = new Book("0000000", true);
    }

    @Test
    public void saveBook_callBookRepositorySaveMethod() throws Exception {
        bookService.save(book);
        verify(bookRepository, times(1)).save(book);
    }

    @Test
    public void deleteBibliographicRecord_callBibliographicRecordRepositoryDeleteMethod() {
        bookService.delete(book);
        verify(bookRepository, times(1)).delete(book);
    }

    @Test
    public void findBookById_callBookRepositoryFindOneMethod() {
        bookService.findById(anyLong());
        verify(bookRepository, times(1)).findOne(anyLong());
    }

    @Test
    public void findBookByBarcode_callBookRepositoryFindByBarcodeMethod() {
        bookService.findByBarcode(anyString());
        verify(bookRepository, times(1)).findByBarcode(anyString());
    }

    @Test
    public void findBookByBarcodeStartingWith_callBookRepositoryFindByBarcodeStartingWithMethod() {
        bookService.findByBarcodeStartingWith(anyString());
        verify(bookRepository, times(1)).findByBarcodeStartingWith(anyString());
    }





}