package com.lorandfazakas.epiclibrary.web.controller;

import static org.junit.Assert.*;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Genre;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;

import java.util.HashSet;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class CatalogControllerTest {

    @Mock
    private BibliographicRecordService bibliographicRecordService;

    @InjectMocks
    private CatalogController catalogController;

    private MockMvc mockMvc;

    Genre genre;

    BibliographicRecord bibliographicRecord;

    @Before
    public void setUp() throws Exception {
        genre = new Genre("Drama");
        bibliographicRecord = new BibliographicRecord("testTitle", "testAuthor", "978-3-16-148410-0", genre);
        bibliographicRecord.setId(1L);
        mockMvc = MockMvcBuilders.standaloneSetup(catalogController)
                .build();
    }

    @Test
    public void testCatalogIndex() throws Exception {
        mockMvc.perform(get("/catalog"))
                .andExpect(status().isOk())
                .andExpect(view().name("catalog/index"));
    }

    @Test
    public void testCatalogIndex2() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("catalog/index"));
    }

    @Test
    public void testCatalogSearch() throws Exception {


        mockMvc.perform(get("/catalog/search"))
                .andExpect(status().isOk())
                .andExpect(model().attributeDoesNotExist("bibliographicRecords"))
                .andExpect(model().attributeDoesNotExist("selectedPageSize"))
                .andExpect(model().attributeDoesNotExist("searchExpression"))
                .andExpect(model().attributeDoesNotExist("pager"))
                .andExpect(view().name("catalog/index"));

    }
}