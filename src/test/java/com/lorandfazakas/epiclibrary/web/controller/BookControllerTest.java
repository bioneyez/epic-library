package com.lorandfazakas.epiclibrary.web.controller;

import static org.junit.Assert.*;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.service.BookService;

import java.util.HashSet;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class BookControllerTest {

    @Mock
    private BookService bookService;

    @Mock
    private BibliographicRecordService bibliographicRecordService;


    private MockMvc mockMvc;

    @InjectMocks
    private BookController bookController;

    Book book;

    BibliographicRecord bibliographicRecord;

    Genre genre;

    @Before
    public void setUp() throws Exception {
        book = new Book("1111111", true);
        book.setId(1L);

        genre = new Genre("Drama");

        bibliographicRecord = new BibliographicRecord("testTitle", "testAuthor", "978-3-16-148410-0", genre);
        bibliographicRecord.setId(1L);

        book.setBibliographicRecord(bibliographicRecord);
        mockMvc = MockMvcBuilders.standaloneSetup(bookController)
                .build();
    }

    @Test
    public void testBookIndex() throws Exception {
        mockMvc.perform(get("/books"))
                .andExpect(status().isOk())
                .andExpect(view().name("book/index"));
    }

    @Test
    public void testBookSearch() throws Exception {
        assertThat(this.bookService).isNotNull();
        HashSet<Book> books = new HashSet<>();
        books.add(book);
        when(bookService.findByBarcodeStartingWith(book.getBarcode())).thenReturn(books);
        mockMvc.perform(get("/books/search?barcode="+book.getBarcode()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("books"))
                .andExpect(model().attribute("books",books))
                .andExpect(model().attribute("books",contains(book)))
                .andExpect(view().name("book/index"));


    }

    @Test
    public void testSingleBookPage() throws Exception {
        assertThat(this.bookService).isNotNull();
        when(bookService.findById(1L)).thenReturn(book);
        mockMvc.perform(get("/books/"+book.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("book/details"))
                .andExpect(model().attributeExists("book"))
                .andExpect(model().attribute("book", hasProperty("barcode", is(book.getBarcode()))))
                .andExpect(model().attribute("book", hasProperty("id", is(book.getId()))))
                .andExpect(model().attribute("book", hasProperty("available", is(book.isAvailable()))));
    }

    @Test
    public void testBookAddPage() throws Exception {

        mockMvc.perform(get("/books/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("book/form"))
                .andExpect(model().attributeExists("action"))
                .andExpect(model().attributeExists("heading"))
                .andExpect(model().attributeExists("submit"))
                .andExpect(model().attribute("action", "/books"))
                .andExpect(model().attribute("heading", "New Book"))
                .andExpect(model().attribute("submit", "Add"));
    }

    @Test
    public void testBookAddPost() throws Exception {
        assertThat(this.bibliographicRecordService).isNotNull();
        when(bibliographicRecordService.findByIsbn(book.getBibliographicRecord().getIsbn())).thenReturn(book.getBibliographicRecord());
        mockMvc.perform(post("/books")
                .param("barcode",book.getBarcode())
                .param("bibliographicRecord.isbn",book.getBibliographicRecord().getIsbn()))
                .andExpect(status().isFound());
        verify(bookService, times(1)).save(any());
    }

    @Test
    public void testBookEditPage() throws Exception {
        assertThat(this.bookService).isNotNull();
        when(bookService.findById(book.getId())).thenReturn(book);
        mockMvc.perform(get("/books/"+ book.getId() + "/edit"))
                .andExpect(status().isOk())
                .andExpect(view().name("book/form"))
                .andExpect(model().attributeExists("book"))
                .andExpect(model().attribute("book", hasProperty("barcode", is(book.getBarcode()))))
                .andExpect(model().attribute("book", hasProperty("available", is(book.isAvailable()))))
                .andExpect(model().attributeExists("action"))
                .andExpect(model().attributeExists("heading"))
                .andExpect(model().attributeExists("submit"))
                .andExpect(model().attribute("action", "/books/"+book.getId()))
                .andExpect(model().attribute("heading", "Edit Book"))
                .andExpect(model().attribute("submit", "Update"));
    }

    @Test
    public void testBookEditPost() throws Exception {
        assertThat(this.bibliographicRecordService).isNotNull();
        when(bibliographicRecordService.findByIsbn(book.getBibliographicRecord().getIsbn())).thenReturn(book.getBibliographicRecord());
        mockMvc.perform(post("/books/" + book.getId())
                .param("barcode",book.getBarcode())
                .param("bibliographicRecord.isbn",bibliographicRecord.getIsbn())
                .param("genre",bibliographicRecord.getGenre().getName()))
                .andExpect(status().isFound());
        verify(bookService, times(1)).save(any());
    }

    @Test
    public void testBookDeletePost() throws Exception {
        assertThat(this.bookService).isNotNull();
        when(bookService.findById(book.getId())).thenReturn(book);
        mockMvc.perform(post("/books/" + book.getId() + "/delete"))
                .andExpect(status().isFound());
        verify(bookService, times(1)).delete(book);
    }


}