package com.lorandfazakas.epiclibrary.web.controller;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.service.LoanService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class MyLendingsControllerTest {

    @Mock
    private LoanService loanService;

    @Mock
    SecurityContext securityContext;

    @Mock
    SecurityContextHolder securityContextHolder;

    @Mock
    Authentication authentication;

    @InjectMocks
    private MyLendingsController myLendingsController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        SecurityContextHolder.setContext(securityContext);
        mockMvc = MockMvcBuilders.standaloneSetup(myLendingsController)
                .build();
    }

    @Test
    public void testGetMessage() throws Exception {
        when(authentication.getName()).thenReturn("test");
        when(securityContext.getAuthentication()).thenReturn(authentication);
        mockMvc.perform(get("/getmessage")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text", is("test")))
                .andExpect(jsonPath("$.subject", is("success")));
    }


}