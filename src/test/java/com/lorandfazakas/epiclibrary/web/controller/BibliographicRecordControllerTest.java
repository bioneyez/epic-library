package com.lorandfazakas.epiclibrary.web.controller;

import static org.junit.Assert.*;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Genre;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;
import com.lorandfazakas.epiclibrary.service.GenreService;
import org.springframework.ui.Model;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.HashSet;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class BibliographicRecordControllerTest {

    @Mock
    private BibliographicRecordService bibliographicRecordService;

    @Mock
    private GenreService genreService;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @InjectMocks
    private BibliographicRecordController bibliographicRecordController;

    BibliographicRecord bibliographicRecord;

    Genre genre;

    @Before
    public void setUp() throws Exception {
        genre = new Genre("Drama");
        bibliographicRecord = new BibliographicRecord("testTitle", "testAuthor", "978-3-16-148410-0", genre);
        bibliographicRecord.setId(1L);
        bibliographicRecord.setBooks(new HashSet<>());
        mockMvc = MockMvcBuilders.standaloneSetup(bibliographicRecordController)
                .build();
    }


    @Test
    public void testBrIndex() throws Exception {

        mockMvc.perform(get("/bibliographicRecords"))
                .andExpect(status().isOk())
                .andExpect(view().name("bibliographic_record/index"));

    }

    @Test
    public void testBrSearch() throws Exception {
        assertThat(this.bibliographicRecordService).isNotNull();
        HashSet<BibliographicRecord> bibliographicRecordHashSet = new HashSet<>();
        bibliographicRecordHashSet.add(bibliographicRecord);
        when(bibliographicRecordService.findByIsbnStartingWith(bibliographicRecord.getIsbn())).thenReturn(bibliographicRecordHashSet);
        mockMvc.perform(get("/bibliographicRecords/search?isbn="+bibliographicRecord.getIsbn()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("bibliographicRecords"))
                .andExpect(model().attribute("bibliographicRecords",bibliographicRecordHashSet))
                .andExpect(model().attribute("bibliographicRecords",contains(bibliographicRecord)))
                .andExpect(view().name("bibliographic_record/index"));
    }

    @Test
    public void testSingleBrPage() throws Exception {
        assertThat(this.bibliographicRecordService).isNotNull();
        when(bibliographicRecordService.findById(bibliographicRecord.getId())).thenReturn(bibliographicRecord);
        mockMvc.perform(get("/bibliographicRecords/"+bibliographicRecord.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("bibliographic_record/details"))
                .andExpect(model().attributeExists("bibliographicRecord"))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("title", is(bibliographicRecord.getTitle()))))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("author", is(bibliographicRecord.getAuthor()))))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("isbn", is(bibliographicRecord.getIsbn()))))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("id", is(bibliographicRecord.getId()))))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("genre", is(bibliographicRecord.getGenre()))));
    }

    @Test
    public void testBrAddPage() throws Exception {
        mockMvc.perform(get("/bibliographicRecords/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("bibliographic_record/form"))
                .andExpect(model().attributeExists("action"))
                .andExpect(model().attributeExists("heading"))
                .andExpect(model().attributeExists("submit"))
                .andExpect(model().attribute("action", "/bibliographicRecords"))
                .andExpect(model().attribute("heading", "New Bibliographic Record"))
                .andExpect(model().attribute("submit", "Add"));
    }

    @Test
    public void testBrAddPost() throws Exception {
        when(genreService.findByName("Drama")).thenReturn(genre);
        mockMvc.perform(post("/bibliographicRecords")
                .param("title",bibliographicRecord.getTitle())
                .param("author",bibliographicRecord.getAuthor())
                .param("genre",bibliographicRecord.getGenre().getName())
                .param("isbn",bibliographicRecord.getIsbn()))
                .andExpect(status().isFound());
        verify(bibliographicRecordService, times(1)).save(any());
    }

    @Test
    public void testBrEditPage() throws Exception {
        when(bibliographicRecordService.findById(bibliographicRecord.getId())).thenReturn(bibliographicRecord);
        mockMvc.perform(get("/bibliographicRecords/"+ bibliographicRecord.getId() + "/edit"))
                .andExpect(status().isOk())
                .andExpect(view().name("bibliographic_record/form"))
                .andExpect(model().attributeExists("bibliographicRecord"))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("title", is(bibliographicRecord.getTitle()))))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("author", is(bibliographicRecord.getAuthor()))))
                .andExpect(model().attribute("bibliographicRecord", hasProperty("isbn", is(bibliographicRecord.getIsbn()))))
                .andExpect(model().attributeExists("action"))
                .andExpect(model().attributeExists("heading"))
                .andExpect(model().attributeExists("submit"))
                .andExpect(model().attribute("action", "/bibliographicRecords/"+bibliographicRecord.getId()))
                .andExpect(model().attribute("heading", "Edit Bibliographic record"))
                .andExpect(model().attribute("submit", "Update"));
    }

    @Test
    public void testBrEditPost() throws Exception {
        when(genreService.findByName("Drama")).thenReturn(genre);
        when(bibliographicRecordService.findById(bibliographicRecord.getId())).thenReturn(bibliographicRecord);
        mockMvc.perform(post("/bibliographicRecords/" + bibliographicRecord.getId())
                .param("title",bibliographicRecord.getTitle())
                .param("author",bibliographicRecord.getAuthor())
                .param("genre",bibliographicRecord.getGenre().getName())
                .param("isbn",bibliographicRecord.getIsbn()))
                .andExpect(status().isFound());
        verify(bibliographicRecordService, times(1)).save(any());
    }

    @Test
    public void testBrDeletePost() throws Exception {
        when(bibliographicRecordService.findById(bibliographicRecord.getId())).thenReturn(bibliographicRecord);
        mockMvc.perform(post("/bibliographicRecords/" + bibliographicRecord.getId() + "/delete"))
                .andExpect(status().isFound());
        verify(bibliographicRecordService, times(1)).delete(bibliographicRecord);
    }

    @Test
    public void testGetGenres() throws Exception {
        ArrayList<Genre> genres = new ArrayList<>();
        genres.add(genre);
        when(genreService.findByNameContains("Drama")).thenReturn(genres);
        mockMvc.perform(get("/genres/getGenres?genreName=Drama")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(genre.getName())));

    }

    @Test
    public void testGetIsbns() throws Exception {
        ArrayList<BibliographicRecord> brs = new ArrayList<>();
        brs.add(bibliographicRecord);
        when(bibliographicRecordService.findByIsbnContains(bibliographicRecord.getIsbn())).thenReturn(brs);
        mockMvc.perform(get("/bibliographicRecords/getIsbns?isbnPart="+bibliographicRecord.getIsbn())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]", is(bibliographicRecord.getIsbn())));
    }

}