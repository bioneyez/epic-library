package com.lorandfazakas.epiclibrary.web.controller;

import static org.junit.Assert.*;

import com.lorandfazakas.epiclibrary.entity.Role;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.enums.RoleEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.service.UserService;

import java.util.HashSet;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class UserControllerTest {

    @Mock
    private UserService userService;

    @Mock
    PasswordEncoder encoder;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    User user;

    @Before
    public void setUp() throws Exception {
        Role role = new Role(RoleEnum.ROLE_USER);
        user = new User("Test Name", "test", "test@test.com", "testpass",role,true);
        user.setId(1L);
        user.setBarcode("1111111");

        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .build();
    }


    @Test
    public void testUserIndex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/index"));
    }


    @Test
    public void testSingleUserPage() throws Exception {
        assertThat(this.userService).isNotNull();
        when(userService.findById(user.getId())).thenReturn(user);
        mockMvc.perform(get("/users/"+ user.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("user/details"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attribute("user", hasProperty("barcode", is(user.getBarcode()))))
                .andExpect(model().attribute("user", hasProperty("id", is(user.getId()))))
                .andExpect(model().attribute("user", hasProperty("name", is(user.getName()))))
                .andExpect(model().attribute("user", hasProperty("username", is(user.getUsername()))))
                .andExpect(model().attribute("user", hasProperty("email", is(user.getEmail()))));
    }

    @Test
    public void testUserPasswordChangePage() throws Exception {
        assertThat(this.userService).isNotNull();
        mockMvc.perform(get("/users/"+ user.getId() + "/passwordChange"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/password_change"))
                .andExpect(model().attributeExists("action"))
                .andExpect(model().attributeExists("heading"))
                .andExpect(model().attributeExists("submit"))
                .andExpect(model().attribute("action", "/users/" + user.getId() + "/passwordChange"))
                .andExpect(model().attribute("heading", "Password Change"))
                .andExpect(model().attribute("submit", "Update"));
    }
    @Test
    public void testUserPasswordChangePost() throws Exception {
        assertThat(this.userService).isNotNull();
        when(userService.findById(user.getId())).thenReturn(user);
        mockMvc.perform(post("/users/" + user.getId() +"/passwordChange")
                .param("password",user.getPassword())
                .param("id", user.getId().toString()))
                .andExpect(status().isFound());
        verify(userService, times(1)).save(any());
        verify(encoder, times(1)).encode(any());
    }



    @Test
    public void testUserAddPage() throws Exception {

        mockMvc.perform(get("/users/add"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/form"))
                .andExpect(model().attributeExists("action"))
                .andExpect(model().attributeExists("heading"))
                .andExpect(model().attributeExists("submit"))
                .andExpect(model().attribute("action", "/users"))
                .andExpect(model().attribute("heading", "New User"))
                .andExpect(model().attribute("submit", "Add"));
    }

    @Test
    public void testUserAddPost() throws Exception {
        assertThat(this.userService).isNotNull();
        mockMvc.perform(post("/users"))
                .andExpect(status().isFound());
        verify(userService, times(1)).register(any());
    }


    @Test
    public void testUserSearch() throws Exception {
        assertThat(this.userService).isNotNull();
        HashSet<User> userHashSet = new HashSet<>();
        userHashSet.add(user);
        when(userService.findByNameStartingWithOrBarcode(user.getBarcode())).thenReturn(userHashSet);
        mockMvc.perform(get("/users/search?name_or_barcode="+user.getBarcode()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users",userHashSet))
                .andExpect(model().attribute("users",contains(user)))
                .andExpect(view().name("user/index"));
    }

    @Test
    public void testUserEditPage() throws Exception {
        assertThat(this.userService).isNotNull();
        when(userService.findById(user.getId())).thenReturn(user);
        mockMvc.perform(get("/users/"+ user.getId() + "/edit"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/form"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attribute("user", hasProperty("name", is(user.getName()))))
                .andExpect(model().attribute("user", hasProperty("id", is(user.getId()))))
                .andExpect(model().attribute("user", hasProperty("barcode", is(user.getBarcode()))))
                .andExpect(model().attribute("user", hasProperty("username", is(user.getUsername()))))
                .andExpect(model().attributeExists("action"))
                .andExpect(model().attributeExists("heading"))
                .andExpect(model().attributeExists("submit"))
                .andExpect(model().attribute("action", "/users/"+user.getId()))
                .andExpect(model().attribute("heading", "Edit User"))
                .andExpect(model().attribute("submit", "Update"));
    }


    @Test
    public void testUserEditPost() throws Exception {
        assertThat(this.userService).isNotNull();
        when(userService.findById(user.getId())).thenReturn(user);
        mockMvc.perform(post("/users/" + user.getId())
                .param("name", user.getName())
                .param("id", user.getId().toString()))
                .andExpect(status().isFound());
        verify(userService, times(1)).save(any());
    }

    @Test
    public void testUserDeletePost() throws Exception {
        assertThat(this.userService).isNotNull();
        when(userService.findById(user.getId())).thenReturn(user);
        mockMvc.perform(post("/users/" + user.getId() + "/delete"))
                .andExpect(status().isFound());
        verify(userService, times(1)).delete(user);
    }


}