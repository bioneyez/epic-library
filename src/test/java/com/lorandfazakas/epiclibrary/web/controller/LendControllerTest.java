package com.lorandfazakas.epiclibrary.web.controller;

import static org.junit.Assert.*;

import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Loan;
import com.lorandfazakas.epiclibrary.entity.Role;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.enums.RoleEnum;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.lorandfazakas.epiclibrary.EpicLibraryApplication;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;
import com.lorandfazakas.epiclibrary.service.BookService;
import com.lorandfazakas.epiclibrary.service.LoanService;
import com.lorandfazakas.epiclibrary.service.UserService;

import java.util.HashSet;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = EpicLibraryApplication.class)
public class LendControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private BookService bookService;

    @Mock
    private LoanService loanService;

    @InjectMocks
    private LendController lendController;

    private MockMvc mockMvc;

    User user;

    Role role;

    Book book;

    @Before
    public void setUp() throws Exception {

        book = new Book("1111111", true);
        book.setId(1L);

        Role role = new Role(RoleEnum.ROLE_USER);
        user = new User("Test Name", "test", "test@test.com", "testpass",role,true);
        user.setId(1L);
        user.setBarcode("1111111");
        book.setLoan(new Loan(user,book));

        mockMvc = MockMvcBuilders.standaloneSetup(lendController)
                .build();
    }

    @Test
    public void testLendIndex() throws Exception {
        mockMvc.perform(get("/lend"))
                .andExpect(status().isOk())
                .andExpect(view().name("lend/index"));
    }

    @Test
    public void testReturnIndex() throws Exception {
        mockMvc.perform(get("/return"))
                .andExpect(status().isOk())
                .andExpect(view().name("lend/return"));
    }

    @Test
    public void testLendSearch() throws Exception {

        assertThat(this.userService).isNotNull();
        HashSet<User> userHashSet = new HashSet<>();
        userHashSet.add(user);
        when(userService.findByNameStartingWithOrBarcode(user.getBarcode())).thenReturn(userHashSet);
        mockMvc.perform(get("/lend/search?name_or_barcode="+user.getBarcode()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users",userHashSet))
                .andExpect(model().attribute("users",contains(user)))
                .andExpect(view().name("lend/index"));
    }

    @Test
    public void testSingleLendPage() throws Exception {
        assertThat(this.userService).isNotNull();
        when(userService.findById(user.getId())).thenReturn(user);
        mockMvc.perform(get("/lend/users/"+user.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("lend/details"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attribute("user", hasProperty("name", is(user.getName()))))
                .andExpect(model().attribute("user", hasProperty("username", is(user.getUsername()))))
                .andExpect(model().attribute("user", hasProperty("id", is(user.getId()))))
                .andExpect(model().attribute("user", hasProperty("barcode", is(user.getBarcode()))))
                .andExpect(model().attribute("user", hasProperty("email", is(user.getEmail()))));
    }

    @Test
    public void testAddLendingPost() throws Exception {
        assertThat(this.userService).isNotNull();
        assertThat(this.bookService).isNotNull();
        when(userService.findById(user.getId())).thenReturn(user);
        when(bookService.findByBarcode(book.getBarcode())).thenReturn(book);
        mockMvc.perform(post("/lend/users/" + user.getId())
                .param("bookBarcode",book.getBarcode()))
                .andExpect(status().isFound());
        verify(userService, times(1)).save(any());
        verify(loanService, times(1)).loanBook(any(), any());
    }

    @Test
    public void testReturnBookSearch() throws Exception {
        assertThat(this.bookService).isNotNull();
        assertThat(this.loanService).isNotNull();

        when(bookService.findByBarcode(book.getBarcode())).thenReturn(book);
        when(loanService.calculateOverdueFee(book.getLoan())).thenReturn(100L);
        mockMvc.perform(get("/return/search?barcode="+book.getBarcode()))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("book"))
                .andExpect(model().attributeExists("overdueFee"))
                .andExpect(model().attribute("book",hasProperty("barcode", is(book.getBarcode()))))
                .andExpect(model().attribute("overdueFee",is(100L)))
                .andExpect(view().name("lend/return"));
    }

    @Test
    public void testReturnLendingPost() throws Exception {
        assertThat(this.bookService).isNotNull();
        assertThat(this.loanService).isNotNull();
        when(bookService.findByBarcode(book.getBarcode())).thenReturn(book);
        mockMvc.perform(post("/return")
                .param("bookBarcode",book.getBarcode()))
                .andExpect(status().isFound());
        verify(loanService, times(1)).returnBook(any());
    }


    @Test
    public void testMyLendingsPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/myLendings"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("lend/my_lendings"));
    }




}