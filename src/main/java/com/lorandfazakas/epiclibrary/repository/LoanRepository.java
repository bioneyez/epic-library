package com.lorandfazakas.epiclibrary.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Loan;
import com.lorandfazakas.epiclibrary.entity.User;

public interface LoanRepository extends JpaRepository<Loan, Long> {
    Set<Loan> findByUser(User user);
    Loan findByBook(Book book);

}
