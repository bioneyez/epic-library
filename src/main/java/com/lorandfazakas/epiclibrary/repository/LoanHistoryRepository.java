package com.lorandfazakas.epiclibrary.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.lorandfazakas.epiclibrary.entity.LoanHistory;

public interface LoanHistoryRepository extends JpaRepository<LoanHistory, Long> {
    
}
