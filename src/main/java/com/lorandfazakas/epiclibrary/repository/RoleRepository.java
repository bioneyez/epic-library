package com.lorandfazakas.epiclibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lorandfazakas.epiclibrary.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
    Role findByName(String name);
}
