package com.lorandfazakas.epiclibrary.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lorandfazakas.epiclibrary.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
    Set<Book> findByBarcodeStartingWith(String barcode);
    Book findByBarcode(String barcode);
}
