package com.lorandfazakas.epiclibrary.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Genre;

public interface BibliographicRecordRepository extends JpaRepository<BibliographicRecord,Long> {
    BibliographicRecord findByIsbn(String isbn);
    Set<BibliographicRecord> findByIsbnStartingWith(String isbn);
    List<BibliographicRecord> findByIsbnContains(String isbn);
    Page<BibliographicRecord> findByTitleContainingOrAuthorContainingOrderByTitle(String title, String author, Pageable pageable);
    Page<BibliographicRecord> findPageableByTitleContainingOrAuthorContainingOrGenreOrderByTitle(String title, String author, Genre genre, Pageable pageable);
}
