package com.lorandfazakas.epiclibrary.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lorandfazakas.epiclibrary.entity.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {
    Genre findByName(String name);
    List<Genre> findByNameContains(String name);
}
