package com.lorandfazakas.epiclibrary.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lorandfazakas.epiclibrary.entity.User;

public interface UserRepository extends JpaRepository<User,Long> {
    User findByEmail(String email);
    User findByUsername(String username);
    User findByBarcode(String barcode);
    Set<User> findByNameStartingWith(String name);
}
