package com.lorandfazakas.epiclibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;




@SpringBootApplication
public class EpicLibraryApplication {

	public static void main(String[] args) {

		SpringApplication.run(EpicLibraryApplication.class, args);



	}
}