package com.lorandfazakas.epiclibrary.web.controller;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lorandfazakas.epiclibrary.exception.BookNotAvailableException;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Loan;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;
import com.lorandfazakas.epiclibrary.service.BookService;
import com.lorandfazakas.epiclibrary.service.LoanService;
import com.lorandfazakas.epiclibrary.service.UserService;
import com.lorandfazakas.epiclibrary.web.FlashMessage;


@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LIBRARIAN')")
@Controller
public class LendController {
    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    @Autowired
    private LoanService loanService;


    @Autowired
    private BibliographicRecordService bibliographicRecordService;

    @GetMapping(path = "/lend")
    public String lendIndex(Model model) {
        return "lend/index";
    }

    @GetMapping(path = "/return")
    public String returnIndex(Model model) {
        return "lend/return";
    }

    @GetMapping(path = "/lend/search")
    public String userSearch(@RequestParam(name = "name_or_barcode", defaultValue = "", required = false) String nameOrBarcode,Model model) {
        Set<User> users = userService.findByNameStartingWithOrBarcode(nameOrBarcode);
        model.addAttribute("users", users);
        return "lend/index";
    }

    @GetMapping("/lend/users/{userId}")
    public String userDetailsForLending(@PathVariable Long userId, Model model) {
        User user = userService.findById(userId);
        model.addAttribute("user", user);
        return "lend/details";
    }

    @PostMapping("/lend/users/{userId}")
    public String lending(@PathVariable Long userId, @RequestParam("bookBarcode") String bookBarcode, Model model, RedirectAttributes redirectAttributes) {
        User user = userService.findById(userId);
        model.addAttribute("user", user);
        Book bookToLoan = bookService.findByBarcode(bookBarcode);
        // Checking loan conditions then trying to lend
        if (bookToLoan == null) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("There is no book with this barcode", FlashMessage.Status.FAILURE));
        } else if (user.getMembershipExpiration().isBefore(LocalDate.now())) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Membership is expired", FlashMessage.Status.FAILURE));
        } else {
            try {
                loanService.loanBook(user, bookToLoan);
                userService.save(user);
                redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book succesfully loaned", FlashMessage.Status.SUCCESS));
            } catch (BookNotAvailableException ex) {
                redirectAttributes.addFlashAttribute("flash", new FlashMessage("This book is not available right now", FlashMessage.Status.FAILURE));

            }
        }
        return String.format("redirect:/lend/users/%s",user.getId());
    }

    @PostMapping("/return")
    public String returning(@RequestParam("bookBarcode") String bookBarcode, Model model, RedirectAttributes redirectAttributes)  {
        Book bookToReturn = bookService.findByBarcode(bookBarcode);
        loanService.returnBook(bookToReturn);
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book successfully returned!", FlashMessage.Status.SUCCESS));
        return "redirect:/return";
    }

    @GetMapping(path = "/return/search")
    public String bookSearch(@RequestParam(name = "barcode", required = false) String barcode,Model model) {
        Book book = bookService.findByBarcode(barcode);
        boolean foundBook = true;
        if (book != null && book.getLoan() != null) {
            model.addAttribute("book", book);
            long overdueFee = loanService.calculateOverdueFee(book.getLoan());
            model.addAttribute("overdueFee", overdueFee);
            foundBook = true;
        } else {
            foundBook = false;
        }
        model.addAttribute("foundBook", foundBook);
        return "lend/return";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LIBRARIAN','ROLE_USER')")
    @GetMapping(path = "/myLendings")
    public String myLendings(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            User loggedInUser = (User) authentication.getPrincipal();
            Set<Loan> loans = loanService.findByUser(loggedInUser);
            model.addAttribute("loans", loans);
        }
        return "lend/my_lendings";
    }



}
