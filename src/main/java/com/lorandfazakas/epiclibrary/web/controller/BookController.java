package com.lorandfazakas.epiclibrary.web.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;
import com.lorandfazakas.epiclibrary.service.BookService;
import com.lorandfazakas.epiclibrary.web.FlashMessage;

@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LIBRARIAN')")
@Controller
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BibliographicRecordService bibliographicRecordService;

    // Book page
    @GetMapping(path = "/books")
    public String index(Model model) {
        return "book/index";
    }

    // Book page search
    @GetMapping(path = "/books/search")
    public String bookSearch(@RequestParam(name = "barcode", defaultValue = "", required = false) String barcode,Model model) {
        Set<Book> books = bookService.findByBarcodeStartingWith(barcode);
        model.addAttribute("books", books);
        return "book/index";
    }

    // Single book page
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LIBRARIAN', 'ROLE_USER')")
    @GetMapping("/books/{bookId}")
    public String book(@PathVariable Long bookId, Model model) {
        Book book = bookService.findById(bookId);
        model.addAttribute("book", book);
        return "book/details";
    }

    // Form for editing an existing book
    @GetMapping("books/{bookId}/edit")
    public String formEditBook(@PathVariable Long bookId, Model model) {
        if (!model.containsAttribute("book")) {
            model.addAttribute("book", bookService.findById(bookId));
        }
        model.addAttribute("action", String.format("/books/%s", bookId));
        model.addAttribute("heading", "Edit Book");
        model.addAttribute("submit", "Update");
        return "book/form";
    }

    // Update an existing book
    @PostMapping(value = "/books/{bookId}")
    public String updateBook(@Valid Book book, BindingResult result, RedirectAttributes redirectAttributes) {
        String redirectPageInCaseOfError = String.format("redirect:/books/%s/edit", book.getId());
        // Update book if valid data was received
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.book", result);
            redirectAttributes.addFlashAttribute("book", book);
            return redirectPageInCaseOfError;
        }

        // Check bibliographic record existence
        BibliographicRecord bibliographicRecord = bibliographicRecordService.findByIsbn(book.getBibliographicRecord().getIsbn());
        if (bibliographicRecord == null) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book update failed. Not found corresponding isbn!", FlashMessage.Status.FAILURE));
            return redirectPageInCaseOfError;
        }
        book.setBibliographicRecord(bibliographicRecord);

        // Try to save to db
        try {
        bookService.save(book);
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book update failed. Barcode is already in use!", FlashMessage.Status.FAILURE));
            redirectAttributes.addFlashAttribute("book", book);
            return redirectPageInCaseOfError;
        }
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book successfully updated!", FlashMessage.Status.SUCCESS));
        return "redirect:/books";
    }


    // Form for adding a new book
    @GetMapping(value = "books/add")
    public String formNewBook(Model model) {
        if (!model.containsAttribute("book")) {
            model.addAttribute("book", new Book());
        }
        model.addAttribute("action", "/books");
        model.addAttribute("heading", "New Book");
        model.addAttribute("submit", "Add");
        return "book/form";
    }


    // Add a Book
    @PostMapping(value = "/books")
    public String addBook(@Valid Book book, BindingResult result, RedirectAttributes redirectAttributes) {
        // Add user if valid data was received
        if (result.hasErrors()) {
            // Include validation errors upon redirect
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.book", result);
            // Add book if invalid data was received
            redirectAttributes.addFlashAttribute("book", book);
            // Redirect back to the form
            return "redirect:/books/add";
        }
        BibliographicRecord bibliographicRecord = bibliographicRecordService.findByIsbn(book.getBibliographicRecord().getIsbn());
        if (bibliographicRecord == null) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book adding failed. Not found corresponding isbn!", FlashMessage.Status.FAILURE));
            redirectAttributes.addFlashAttribute("book", book);
            return "redirect:/books/add";
        }
        book.setBibliographicRecord(bibliographicRecord);
        book.setAvailable(true);
        // Try to save to DB
        try {
            bookService.save(book);
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book adding failed. Barcode is already in use!", FlashMessage.Status.FAILURE));
            redirectAttributes.addFlashAttribute("book", book);
            return "redirect:/books/add";
        }
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book successfully added!", FlashMessage.Status.SUCCESS));
        return String.format("redirect:/bibliographicRecords/%s", book.getBibliographicRecord().getId());
    }

    // Delete an existing book
    @PostMapping(value = "/books/{bookId}/delete")
    public String deleteBook(@PathVariable Long bookId, RedirectAttributes redirectAttributes) {
        Book book = bookService.findById(bookId);
        try {
            bookService.delete(book);
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book deletion failed. Book is loaned.", FlashMessage.Status.FAILURE));
            return String.format("redirect:/books/%s/edit", book.getId());
        }
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Book deleted", FlashMessage.Status.SUCCESS));
        return "redirect:/books";
    }
}
