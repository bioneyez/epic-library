package com.lorandfazakas.epiclibrary.web.controller;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.service.UserService;
import com.lorandfazakas.epiclibrary.web.FlashMessage;

@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LIBRARIAN')")
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    PasswordEncoder encoder;


    @GetMapping(path = "/users")
    public String index(Model model) {
        return "user/index";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("users/{userId}/passwordChange")
    public String formChangeUserPassword(@PathVariable Long userId, Model model) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", userService.findById(userId));
        }
        model.addAttribute("action", String.format("/users/%s/passwordChange", userId));
        model.addAttribute("heading", "Password Change");
        model.addAttribute("submit", "Update");
        return "user/password_change";
    }

    // Change an existing users password
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/users/{userId}/passwordChange")
    public String changeUserPassword(@Valid User user, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
            redirectAttributes.addFlashAttribute("user", user);
            return String.format("redirect:/users/%s/passwordChange", user.getId());
        }
        User userIndb = userService.findById(user.getId());
        userIndb.setPassword(encoder.encode(user.getPassword()));
        userService.save(userIndb);
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("User successfully updated!", FlashMessage.Status.SUCCESS));
        return "redirect:/users";
    }

    // Form for editing an existing user
    @GetMapping("users/{userId}/edit")
    public String formEditUser(@PathVariable Long userId, Model model) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", userService.findById(userId));
        }
        model.addAttribute("action", String.format("/users/%s", userId));
        model.addAttribute("heading", "Edit User");
        model.addAttribute("submit", "Update");
        return "user/form";
    }

    // Update an existing user
    @PostMapping(value = "/users/{userId}")
    public String updateUser(@Valid User user, BindingResult result, RedirectAttributes redirectAttributes) {
        // Check if valid data was received
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
            redirectAttributes.addFlashAttribute("user", user);
            return String.format("redirect:/users/%s/edit", user.getId());
        }
        // this is necessary because password does not come from form, set it to the current password
        user.setPassword(userService.findById(user.getId()).getPassword());
        try {
            userService.save(user);
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("User update failed! Barcode is already in use.", FlashMessage.Status.FAILURE));
            return String.format("redirect:/users/%s/edit", user.getId());
        }
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("User successfully updated!", FlashMessage.Status.SUCCESS));
        return "redirect:/users";
    }

    // Form for adding a new user
    @GetMapping(value = "users/add")
    public String formNewUser(Model model) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new User());
        }
        model.addAttribute("action", "/users");
        model.addAttribute("heading", "New User");
        model.addAttribute("submit", "Add");
        return "user/form";
    }


    // Add a User
    @PostMapping(value = "/users")
    public String addUser(@Valid User user, BindingResult result, RedirectAttributes redirectAttributes) {
        // Add user if valid data was received
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
            redirectAttributes.addFlashAttribute("user", user);
            return "redirect:/users/add";
        }
        // try to save user to db
        try {
            userService.register(user);
        } catch (DataIntegrityViolationException ex) {
            if (userService.findByUsername(user.getUsername()) != null) {
                redirectAttributes.addFlashAttribute("flash", new FlashMessage("Username is already taken!", FlashMessage.Status.FAILURE));
            }
            else if (userService.findByBarcode(user.getBarcode())!= null) {
                redirectAttributes.addFlashAttribute("flash", new FlashMessage("Barcode is already in use!", FlashMessage.Status.FAILURE));
            }
            redirectAttributes.addFlashAttribute("user", user);
            return "redirect:/users/add";
        }
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("User successfully added!", FlashMessage.Status.SUCCESS));
        return "redirect:/users";
    }

    // Index for user search and listing
    @GetMapping(path = "/users/search")
    public String userSearch(@RequestParam(name = "name_or_barcode", defaultValue = "", required = false) String nameOrBarcode,Model model) {
        Set<User> users = userService.findByNameStartingWithOrBarcode(nameOrBarcode);
        model.addAttribute("users", users);
        return "user/index";
    }


    // Single user page
    @GetMapping("/users/{userId}")
    public String user(@PathVariable Long userId, Model model) {
        User user = userService.findById(userId);
        model.addAttribute("user", user);
        return "user/details";
    }

    // Delete an existing user
    @PostMapping(value = "/users/{userId}/delete")
    public String deleteUser(@PathVariable Long userId, RedirectAttributes redirectAttributes) {
        User user = userService.findById(userId);
        if(user.getLoans().size() > 0) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("User cannot be deleted because he/she has borrowed books",FlashMessage.Status.FAILURE));
            return String.format("redirect:/users/%s/edit", userId);
        }
        userService.delete(user);
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("User deleted", FlashMessage.Status.SUCCESS));
        return "redirect:/users";
    }

    @PostMapping("/users/{userId}/renew")
    public String membershipRenew(@PathVariable Long userId, Model model, RedirectAttributes redirectAttributes) {
        User user = userService.findById(userId);
        userService.renewMembership(user);
        model.addAttribute("user", user);
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Membership renewal successfull", FlashMessage.Status.SUCCESS));

        return String.format("redirect:/users/%s",user.getId());
    }
}
