package com.lorandfazakas.epiclibrary.web.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;
import com.lorandfazakas.epiclibrary.web.Pager;

@Controller
public class CatalogController {

    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 5;

    @Autowired
    private BibliographicRecordService bibliographicRecordService;

    // Catalog index
    @GetMapping(path = {"/","/catalog"})
    public String catalogIndex(Model model) {
        return "catalog/index";
    }

    // Search in catalog
    @GetMapping(path = "/catalog/search")
    public String catalogSearch(@RequestParam(name = "searchExpression", defaultValue = "", required = false) String searchExpression, Model model,@RequestParam("pageSize") Optional<Integer> pageSize,
            @RequestParam("page") Optional<Integer> page) {
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<BibliographicRecord> bibliographicRecords =
                bibliographicRecordService.findPageableByTitleContainingOrAuthorContainingOrGenreOrderByTitle(searchExpression,searchExpression,searchExpression,new PageRequest(evalPage,evalPageSize));
        if (bibliographicRecords != null) {
            Pager pager = new Pager(bibliographicRecords.getTotalPages(), bibliographicRecords.getNumber(), BUTTONS_TO_SHOW);

            model.addAttribute("bibliographicRecords", bibliographicRecords);
            model.addAttribute("searchExpression", searchExpression);
            model.addAttribute("selectedPageSize", evalPageSize);
            model.addAttribute("pager",pager);
        }
        return "catalog/index";
    }
}
