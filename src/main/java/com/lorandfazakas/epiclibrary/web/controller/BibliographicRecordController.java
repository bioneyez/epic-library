package com.lorandfazakas.epiclibrary.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;
import com.lorandfazakas.epiclibrary.service.GenreService;
import com.lorandfazakas.epiclibrary.web.FlashMessage;


@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LIBRARIAN')")
@Controller
public class BibliographicRecordController {

    @Autowired
    private BibliographicRecordService bibliographicRecordService;

    @Autowired
    private GenreService genreService;

    // Bibliographic Record page
    @GetMapping(path = "/bibliographicRecords")
    public String index(Model model) {
        return "bibliographic_record/index";
    }

    // Bibliographic Record  page search
    @GetMapping(path = "/bibliographicRecords/search")
    public String bibliographicRecordSearch(@RequestParam(value = "isbn", required = false, defaultValue = "") String isbn,Model model) {
        Set<BibliographicRecord> bibliographicRecords = bibliographicRecordService.findByIsbnStartingWith(isbn);
        model.addAttribute("bibliographicRecords", bibliographicRecords);
        return "bibliographic_record/index";
    }

    // Single bibliographic record page
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LIBRARIAN', 'ROLE_USER')")
    @GetMapping("/bibliographicRecords/{bibliographicRecordId}")
    public String bibliographicRecord(@PathVariable Long bibliographicRecordId, Model model) {
        BibliographicRecord bibliographicRecord = bibliographicRecordService.findById(bibliographicRecordId);
        model.addAttribute("bibliographicRecord", bibliographicRecord);
        return "bibliographic_record/details";
    }


    // New Bibliographic Record form
    @GetMapping(value = "/bibliographicRecords/add")
    public String formNewBibliographicRecord(Model model) {
        // Add entity attributes needed for new form
        if (!model.containsAttribute("bibliographicRecord")) {
            model.addAttribute("bibliographicRecord", new BibliographicRecord());
        }

        model.addAttribute("action", "/bibliographicRecords");
        model.addAttribute("heading", "New Bibliographic Record");
        model.addAttribute("submit", "Add");
        return "bibliographic_record/form";
    }


    // Add a bibliographic record
    @PostMapping(value = "/bibliographicRecords")
    public String addBibliographicRecord(@Valid BibliographicRecord bibliographicRecord, BindingResult result, RedirectAttributes redirectAttributes) {
        String redirectPageInCaseOfError = "redirect:/bibliographicRecords/add";
        // Check if valid data was received
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.bibliographicRecord", result);
            redirectAttributes.addFlashAttribute("bibliographicRecord", bibliographicRecord);
            return redirectPageInCaseOfError;
        }
        // Check if genre exists
        Genre genre = genreService.findByName(bibliographicRecord.getGenre().getName());
        if (genre == null) {
            redirectAttributes.addFlashAttribute("bibliographicRecord", bibliographicRecord);
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("There is no genre named like this", FlashMessage.Status.FAILURE));
            return redirectPageInCaseOfError;
        }
        bibliographicRecord.setGenre(genre);
        // Try to save to DB
        try {
            bibliographicRecordService.save(bibliographicRecord);

        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("ISBN is already in the database!", FlashMessage.Status.FAILURE));
            redirectAttributes.addFlashAttribute("bibliographicRecord", bibliographicRecord);
            return redirectPageInCaseOfError;
        }
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Bibliographic record successfully added!", FlashMessage.Status.SUCCESS));

        return "redirect:/bibliographicRecords";
    }


    // Form for editing an existing bibliographic record
    @GetMapping("bibliographicRecords/{bibliographicRecordId}/edit")
    public String formEditBibliographicRecord(@PathVariable Long bibliographicRecordId, Model model) {
        if (!model.containsAttribute("bibliographicRecord")) {
            model.addAttribute("bibliographicRecord", bibliographicRecordService.findById(bibliographicRecordId));
        }
        model.addAttribute("action", String.format("/bibliographicRecords/%s", bibliographicRecordId));
        model.addAttribute("heading", "Edit Bibliographic record");
        model.addAttribute("submit", "Update");
        return "bibliographic_record/form";
    }

    // Update an existing bibliographic record
    @PostMapping(value = "/bibliographicRecords/{bibliographicRecordId}")
    public String updateBibliographicRecord(@Valid BibliographicRecord bibliographicRecord, BindingResult result, RedirectAttributes redirectAttributes) {
        String redirectPageInCaseOfError = String.format("redirect:/bibliographicRecords/%s/edit", bibliographicRecord.getId());
        // Check if valid data was received
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.bibliographicRecord", result);
            redirectAttributes.addFlashAttribute("bibliographicRecord", bibliographicRecord);
            return redirectPageInCaseOfError;
        }
        // Check if genre exists
        Genre genre = genreService.findByName(bibliographicRecord.getGenre().getName());
        if (genre == null) {
            redirectAttributes.addFlashAttribute("bibliographicRecord", bibliographicRecord);
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("There is no genre named like this", FlashMessage.Status.FAILURE));
            return redirectPageInCaseOfError;
        }
        bibliographicRecord.setGenre(genre);
        // Try to save to DB
        try {
            bibliographicRecordService.save(bibliographicRecord);
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("This ISBN is already taken!", FlashMessage.Status.FAILURE));
            redirectAttributes.addFlashAttribute("bibliographicRecord", bibliographicRecord);
            return redirectPageInCaseOfError;
        }
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Bibliographic record successfully updated!", FlashMessage.Status.SUCCESS));
        return "redirect:/bibliographicRecords";
    }


    // Delete an existing bibliographic record
    @PostMapping(value = "/bibliographicRecords/{bibliographicRecordId}/delete")
    public String deleteBibliographicRecord(@PathVariable Long bibliographicRecordId, RedirectAttributes redirectAttributes) {
        BibliographicRecord bibliographicRecord = bibliographicRecordService.findById(bibliographicRecordId);
        if(bibliographicRecord.getBooks().size() > 0) {
            redirectAttributes.addFlashAttribute("flash", new FlashMessage("Bibliographic record cannot be deleted because it has associated books",FlashMessage.Status.FAILURE));
            return String.format("redirect:/bibliographicRecords/%s/edit", bibliographicRecordId);
        }
        bibliographicRecordService.delete(bibliographicRecord);
        redirectAttributes.addFlashAttribute("flash", new FlashMessage("Bibliographic record deleted", FlashMessage.Status.SUCCESS));
        return "redirect:/bibliographicRecords";
    }

    // Produces genres as JSON
    @GetMapping(value = "genres/getGenres")
    @ResponseBody
    public List<Genre> getGenres(@RequestParam String genreName) {
        return genreService.findByNameContains(genreName);

    }


    // Produces ISBN-s as JSON
    @GetMapping(value = "bibliographicRecords/getIsbns")
    @ResponseBody
    public List<String> getIsbns(@RequestParam String isbnPart) {
        List<BibliographicRecord> bibliographicRecords = bibliographicRecordService.findByIsbnContains(isbnPart);
        List<String> result = new ArrayList<>();
        for (BibliographicRecord bibliographicRecord: bibliographicRecords) {
            result.add(bibliographicRecord.getIsbn());
        }
        return result;

    }
}
