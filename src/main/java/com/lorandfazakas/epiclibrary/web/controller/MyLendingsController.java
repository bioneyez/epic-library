package com.lorandfazakas.epiclibrary.web.controller;

import com.lorandfazakas.epiclibrary.dto.Message;
import com.lorandfazakas.epiclibrary.dto.MyLendingsDto;
import com.lorandfazakas.epiclibrary.service.LoanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyLendingsController {

    @Autowired
    private LoanService loanService;

    private static final Logger logger = LoggerFactory.getLogger(MyLendingsController.class);

    @RequestMapping(value = "/getmessage", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Message getMessage() {
        logger.info("Accessing protected resource");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return new Message(100, "success", username);
    }

    @RequestMapping(value = "/getmylendings", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody MyLendingsDto getMyLendings() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        MyLendingsDto myLendingsDto = new MyLendingsDto(loanService.getMyLendings(username));
        return myLendingsDto;
    }


}
