package com.lorandfazakas.epiclibrary.web;

public class Pager {
    private int buttonsToShow;

    private int startPage;

    private int endPage;

    public Pager(int totalPages, int currentPage, int buttonsToShow) {
        this.buttonsToShow = buttonsToShow;
        this.startPage = 1;
        this.endPage = totalPages;

    }

    public int getButtonsToShow() {
        return buttonsToShow;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

}
