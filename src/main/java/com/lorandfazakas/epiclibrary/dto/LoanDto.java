package com.lorandfazakas.epiclibrary.dto;

import java.time.LocalDate;

public class LoanDto implements Comparable<LoanDto>{
    private String title;
    private String author;
    private String barcode;
    private long overdueFee;
    private LocalDate returnDeadline;

    public LoanDto(String title, String author, String barcode, long overdueFee, LocalDate returnDeadline) {
        this.title = title;
        this.author = author;
        this.barcode = barcode;
        this.overdueFee = overdueFee;
        this.returnDeadline = returnDeadline;
    }

    public LoanDto() {
    }

    public LocalDate getReturnDeadline() {
        return returnDeadline;
    }

    public void setReturnDeadline(LocalDate returnDeadline) {
        this.returnDeadline = returnDeadline;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public long getOverdueFee() {
        return overdueFee;
    }

    public void setOverdueFee(long overdueFee) {
        this.overdueFee = overdueFee;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public int compareTo(LoanDto o) {
        if (this.returnDeadline.equals(o.returnDeadline)) {
            return this.title.compareTo(o.title);
        }
        return this.returnDeadline.compareTo(o.returnDeadline);
    }
}
