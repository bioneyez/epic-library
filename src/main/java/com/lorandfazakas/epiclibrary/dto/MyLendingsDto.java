package com.lorandfazakas.epiclibrary.dto;

import java.util.List;

public class MyLendingsDto {
    private List<LoanDto> lendings;

    public MyLendingsDto(List<LoanDto> lendings) {
        this.lendings = lendings;
    }

    public List<LoanDto> getLendings() {
        return lendings;
    }

    public void setLendings(List<LoanDto> lendings) {
        this.lendings = lendings;
    }
}
