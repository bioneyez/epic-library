package com.lorandfazakas.epiclibrary.enums;

public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_LIBRARIAN,
    ROLE_USER
}
