package com.lorandfazakas.epiclibrary.service;

import java.util.Set;

import com.lorandfazakas.epiclibrary.entity.User;

public interface UserService {
    void save(User user);
    void delete(User user);
    User findByUsername(String username);
    User findByEmail(String email);
    User findById(Long id);
    User findByBarcode(String barcode);
    Set<User> findByNameStartingWith(String name);
    Set<User> findByNameStartingWithOrBarcode(String nameOrBarcode);
    void register(User user);
    void renewMembership(User user);

}
