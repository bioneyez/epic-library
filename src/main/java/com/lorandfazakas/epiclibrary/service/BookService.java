package com.lorandfazakas.epiclibrary.service;

import java.util.Set;

import com.lorandfazakas.epiclibrary.entity.Book;

public interface BookService {
    void save(Book book);
    void delete(Book book);
    Set<Book> findByBarcodeStartingWith(String barcode);
    Book findByBarcode(String barcode);
    Book findById(Long id);
}
