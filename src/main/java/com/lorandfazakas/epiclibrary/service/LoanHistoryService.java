package com.lorandfazakas.epiclibrary.service;

import com.lorandfazakas.epiclibrary.entity.LoanHistory;

public interface LoanHistoryService {
    void save(LoanHistory loanHistory);
    void delete(LoanHistory loanHistory);
    LoanHistory findById(Long id);
}
