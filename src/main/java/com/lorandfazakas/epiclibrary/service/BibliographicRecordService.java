package com.lorandfazakas.epiclibrary.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;

public interface BibliographicRecordService {
    void save(BibliographicRecord bibliographicRecord);
    void delete(BibliographicRecord bibliographicRecord);
    BibliographicRecord findById(Long id);
    BibliographicRecord findByIsbn(String isbn);
    Set<BibliographicRecord> findByIsbnStartingWith(String isbn);
    List<BibliographicRecord> findByIsbnContains(String isbn);
    Page<BibliographicRecord> findPageableByTitleContainingOrAuthorContainingOrderByTitle(String searchWordForTitle, String searchWordForAuthor, Pageable pageable);
    Page<BibliographicRecord> findPageableByTitleContainingOrAuthorContainingOrGenreOrderByTitle(String searchWordForTitle, String searchWordForAuthor,String searchWordForGenre, Pageable pageable);
}
