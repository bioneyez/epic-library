package com.lorandfazakas.epiclibrary.service;

import java.util.List;
import java.util.Set;

import com.lorandfazakas.epiclibrary.dto.LoanDto;
import com.lorandfazakas.epiclibrary.exception.BookNotAvailableException;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Loan;
import com.lorandfazakas.epiclibrary.entity.User;

public interface LoanService {
    void save(Loan loan);
    void delete(Loan loan);
    Loan findById(Long id);
    Set<Loan> findByUser(User user);
    void loanBook(User user, Book book) throws BookNotAvailableException;
    void returnBook(Book book);
    long calculateOverdueFee(Loan loan);
    List<LoanDto> getMyLendings(String username);
}
