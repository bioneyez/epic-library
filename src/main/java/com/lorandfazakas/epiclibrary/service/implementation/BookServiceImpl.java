package com.lorandfazakas.epiclibrary.service.implementation;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.repository.BookRepository;
import com.lorandfazakas.epiclibrary.service.BookService;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void delete(Book book) {
        bookRepository.delete(book);
    }

    @Override
    public Set<Book> findByBarcodeStartingWith(String barcode) {
        return bookRepository.findByBarcodeStartingWith(barcode);
    }

    @Override
    public Book findByBarcode(String barcode) {
        return bookRepository.findByBarcode(barcode);
    }

    @Override
    public Book findById(Long id) {
        return bookRepository.findOne(id);
    }

}
