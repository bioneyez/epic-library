package com.lorandfazakas.epiclibrary.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lorandfazakas.epiclibrary.entity.LoanHistory;
import com.lorandfazakas.epiclibrary.repository.LoanHistoryRepository;
import com.lorandfazakas.epiclibrary.service.LoanHistoryService;

@Service
public class LoanHistoryServiceImpl implements LoanHistoryService {

    @Autowired
    private LoanHistoryRepository loanHistoryRepository;

    @Override
    public void save(LoanHistory loanHistory) {
        loanHistoryRepository.save(loanHistory);
    }

    @Override
    public void delete(LoanHistory loanHistory) {
        loanHistoryRepository.delete(loanHistory);
    }

    @Override
    public LoanHistory findById(Long id) {
        return loanHistoryRepository.findOne(id);
    }
}
