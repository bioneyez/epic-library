package com.lorandfazakas.epiclibrary.service.implementation;

import java.util.List;
import java.util.Set;

import com.lorandfazakas.epiclibrary.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.repository.BibliographicRecordRepository;
import com.lorandfazakas.epiclibrary.service.BibliographicRecordService;

@Service
public class BibliographicRecordServiceImpl implements BibliographicRecordService {

    @Autowired
    private BibliographicRecordRepository bibliographicRecordRepository;

    @Autowired
    private GenreService genreService;



    @Override
    public void save(BibliographicRecord bibliographicRecord) {
        bibliographicRecordRepository.save(bibliographicRecord);
    }

    @Override
    public void delete(BibliographicRecord bibliographicRecord) {
        bibliographicRecordRepository.delete(bibliographicRecord);
    }

    @Override
    public BibliographicRecord findById(Long id) {
        return bibliographicRecordRepository.findOne(id);
    }

    @Override
    public BibliographicRecord findByIsbn(String isbn) {
        return bibliographicRecordRepository.findByIsbn(isbn);
    }

    @Override
    public Set<BibliographicRecord> findByIsbnStartingWith(String isbn) {
        return bibliographicRecordRepository.findByIsbnStartingWith(isbn);
    }

    @Override
    public Page<BibliographicRecord> findPageableByTitleContainingOrAuthorContainingOrderByTitle(String searchWordForTitle, String searchWordForAuthor, Pageable pageable) {
        return bibliographicRecordRepository.findByTitleContainingOrAuthorContainingOrderByTitle(searchWordForTitle,searchWordForAuthor,pageable);
    }

    @Override
    public Page<BibliographicRecord> findPageableByTitleContainingOrAuthorContainingOrGenreOrderByTitle(String searchWordForTitle, String searchWordForAuthor, String searchWordForGenre, Pageable pageable) {
        Genre genre = genreService.findByName(searchWordForGenre);
        if (genre != null) {
            return bibliographicRecordRepository.findPageableByTitleContainingOrAuthorContainingOrGenreOrderByTitle(searchWordForTitle, searchWordForAuthor, genre, pageable);
        }
        return findPageableByTitleContainingOrAuthorContainingOrderByTitle(searchWordForTitle,searchWordForAuthor,pageable);
    }

    @Override
    public List<BibliographicRecord> findByIsbnContains(String isbn) {
        return bibliographicRecordRepository.findByIsbnContains(isbn);
    }
}
