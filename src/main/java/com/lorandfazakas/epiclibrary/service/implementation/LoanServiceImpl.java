package com.lorandfazakas.epiclibrary.service.implementation;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.lorandfazakas.epiclibrary.dto.LoanDto;
import com.lorandfazakas.epiclibrary.service.BookService;
import com.lorandfazakas.epiclibrary.service.LoanHistoryService;
import com.lorandfazakas.epiclibrary.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lorandfazakas.epiclibrary.exception.BookNotAvailableException;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Loan;
import com.lorandfazakas.epiclibrary.entity.LoanHistory;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.repository.BookRepository;
import com.lorandfazakas.epiclibrary.repository.LoanHistoryRepository;
import com.lorandfazakas.epiclibrary.repository.LoanRepository;
import com.lorandfazakas.epiclibrary.repository.UserRepository;
import com.lorandfazakas.epiclibrary.service.LoanService;

@Service
public class LoanServiceImpl implements LoanService {

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private LoanHistoryService loanHistoryService;

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Override
    public void save(Loan loan) {
        loanRepository.save(loan);
    }

    @Override
    public void delete(Loan loan) {
        loanRepository.delete(loan);
    }

    @Override
    public Loan findById(Long id) {
        return loanRepository.findOne(id);
    }

    @Override
    public Set<Loan> findByUser(User user) {
        return loanRepository.findByUser(user);
    }

    @Override
    public void loanBook(User user, Book book) throws BookNotAvailableException {
        if (book.isAvailable()) {
            book.setAvailable(false);
            Loan loan = new Loan(user,book);
            user.newLoan(loan);
            save(loan);
        } else {
            throw new BookNotAvailableException("Book is not available");
        }
    }

    @Override
    public void returnBook(Book returnBook) {
        // Get corresponding loan
        Loan loan = loanRepository.findByBook(returnBook);
        if (loan != null) {
            loan.setReturnDate(LocalDate.now());
            // Make book available again
            Book book = loan.getBook();
            book.setAvailable(true);
            bookService.save(book);
            // Make it history
            LoanHistory loanHistory = new LoanHistory(loan);
            loanHistoryService.save(loanHistory);
            // Remove from active loans
            loan.getUser().removeLoan(loan);
            delete(loan);
        }
    }

    @Override
    public long calculateOverdueFee(Loan loan) {
        return loan.calculateOverdueFee();
    }

    @Override
    public List<LoanDto> getMyLendings(String username) {
        List<LoanDto> myLendings = new ArrayList<>();
        User user = userService.findByUsername(username);
        if (user != null) {
            Set<Loan> loans = user.getLoans();
            for (Loan loan: loans) {
                myLendings.add(loan2LoanDto(loan));
            }
        }
        Collections.sort(myLendings);
        return myLendings;
    }

    private LoanDto loan2LoanDto(Loan loan) {
        Book book = loan.getBook();
        LoanDto loanDto = new LoanDto();
        loanDto.setAuthor(book.getBibliographicRecord().getAuthor());
        loanDto.setTitle(book.getBibliographicRecord().getTitle());
        loanDto.setBarcode(book.getBarcode());
        loanDto.setOverdueFee(loan.calculateOverdueFee());
        loanDto.setReturnDeadline(loan.getReturnDateDeadline());
        return loanDto;
    }
}
