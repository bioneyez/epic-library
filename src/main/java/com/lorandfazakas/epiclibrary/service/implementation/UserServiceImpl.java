package com.lorandfazakas.epiclibrary.service.implementation;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.lorandfazakas.epiclibrary.configuration.LibraryConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.repository.RoleRepository;
import com.lorandfazakas.epiclibrary.repository.UserRepository;
import com.lorandfazakas.epiclibrary.service.UserService;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;


    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void register(User user) {
        user.setEnabled(true);
        user.setRole(roleRepository.findByName("ROLE_USER"));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findByBarcode(String barcode) {
        return userRepository.findByBarcode(barcode);
    }


    @Override
    public Set<User> findByNameStartingWith(String name) {
        return userRepository.findByNameStartingWith(name);
    }

    @Override
    public Set<User> findByNameStartingWithOrBarcode(String nameOrBarcode) {
        boolean isNumeric = nameOrBarcode.chars().allMatch( Character::isDigit );
        Set<User> users = new HashSet<>();
        if (isNumeric) {
            User user = userRepository.findByBarcode(nameOrBarcode);
            if (user != null) {
                users.add(user);
            }

        } else {
            users.addAll(userRepository.findByNameStartingWith(nameOrBarcode));
        }
        return users;
    }

    @Override
    public void renewMembership(User user) {
        user.setMembershipExpiration(LocalDate.now().plusDays(LibraryConfiguration.getMembershipExtendInDays()));
        userRepository.save(user);
    }
}
