package com.lorandfazakas.epiclibrary.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.repository.GenreRepository;
import com.lorandfazakas.epiclibrary.service.GenreService;

@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Override
    public Genre findById(Long id) {
        return genreRepository.findOne(id);
    }



    @Override
    public List<Genre> findByNameContains(String name) {
        return genreRepository.findByNameContains(name);
    }

    @Override
    public Genre findByName(String name) {
        return genreRepository.findByName(name);
    }
}
