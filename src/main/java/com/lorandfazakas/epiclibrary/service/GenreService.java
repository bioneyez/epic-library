package com.lorandfazakas.epiclibrary.service;

import java.util.List;

import com.lorandfazakas.epiclibrary.entity.Genre;

public interface GenreService {
    Genre findById(Long id);
    Genre findByName(String name);
    List<Genre> findByNameContains(String name);
}
