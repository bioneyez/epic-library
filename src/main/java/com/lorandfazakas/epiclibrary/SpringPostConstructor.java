package com.lorandfazakas.epiclibrary;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.lorandfazakas.epiclibrary.configuration.LibraryConfiguration;
import com.lorandfazakas.epiclibrary.entity.BibliographicRecord;
import com.lorandfazakas.epiclibrary.entity.Book;
import com.lorandfazakas.epiclibrary.entity.Genre;
import com.lorandfazakas.epiclibrary.entity.Role;
import com.lorandfazakas.epiclibrary.entity.User;
import com.lorandfazakas.epiclibrary.repository.BibliographicRecordRepository;
import com.lorandfazakas.epiclibrary.repository.BookRepository;
import com.lorandfazakas.epiclibrary.repository.GenreRepository;
import com.lorandfazakas.epiclibrary.repository.RoleRepository;
import com.lorandfazakas.epiclibrary.repository.UserRepository;
import com.lorandfazakas.epiclibrary.service.LoanService;

@Component
public class SpringPostConstructor {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BibliographicRecordRepository bibliographicRecordRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private LoanService loanService;

    @Autowired
    PasswordEncoder PASSWORD_ENCODER;

    @Autowired
    Environment environment;

    @PostConstruct
    public void loadTestDataToDB() {
        fillRoleTable();
        fillGenreTable();
        fillUserTable();
        fillBibliographicRecordTable();
        fillBookTable();
        printSettings();
    }

    public void fillRoleTable() {
        if (roleRepository.findAll().isEmpty()) {
            Role role = new Role();
            role.setName("ROLE_ADMIN");
            roleRepository.save(role);

            role = new Role();
            role.setName("ROLE_USER");
            roleRepository.save(role);

            role = new Role();
            role.setName("ROLE_LIBRARIAN");
            roleRepository.save(role);

        }

    }


    public void fillUserTable(){
        if (userRepository.findByEmail("admin@admin.com") == null) {

            User user = new User();
            user.setEmail("admin@admin.com");
            user.setName("Admin János");
            user.setUsername("admin");
            user.setPassword(PASSWORD_ENCODER.encode("admin"));
            user.setRole(roleRepository.findByName("ROLE_ADMIN"));
            user.setEnabled(true);
            user.setBarcode("1111111");
            user.setZipCode("2040");
            user.setCity("Budaörs");
            user.setStreetAddress("Szivárvány út 37");
            userRepository.save(user);
        }

        if (userRepository.findByEmail("user@user.com") == null) {

            User user = new User();
            user.setEmail("user@user.com");
            user.setName("User Botond");
            user.setUsername("user");
            user.setPassword(PASSWORD_ENCODER.encode("user"));
            user.setRole(roleRepository.findByName("ROLE_USER"));
            user.setEnabled(true);
            user.setBarcode("2222222");
            user.setZipCode("1091");
            user.setCity("Budapest");
            user.setStreetAddress("Nagy utca 26");
            userRepository.save(user);
        }

        if (userRepository.findByEmail("user1@user.com") == null) {

            User user = new User();
            user.setEmail("user1@user.com");
            user.setName("User Balázs");
            user.setPassword(PASSWORD_ENCODER.encode("user"));
            user.setUsername("user1");
            user.setRole(roleRepository.findByName("ROLE_USER"));
            user.setEnabled(true);
            user.setBarcode("3333333");
            user.setZipCode("1091");
            user.setCity("Budapest");
            user.setStreetAddress("Kis utca 16");
            user.setMembershipExpiration(LocalDate.now().minusDays(70));
            userRepository.save(user);
        }

        if (userRepository.findByEmail("lib@lib.com") == null) {

            User user = new User();
            user.setEmail("lib@lib.com");
            user.setName("Librarian Erzsébet");
            user.setUsername("librarian");
            user.setPassword(PASSWORD_ENCODER.encode("librarian"));
            user.setRole(roleRepository.findByName("ROLE_LIBRARIAN"));
            user.setEnabled(true);
            user.setBarcode("4444444");
            user.setZipCode("1091");
            user.setCity("Budapest");
            user.setStreetAddress("Próba utca 76/B");
            userRepository.save(user);
        }
        if (userRepository.findByEmail("lib1@lib.com") == null) {

            User user = new User();
            user.setEmail("lib1@lib.com");
            user.setName("Librarian Magdolna");
            user.setUsername("librarian1");
            user.setPassword(PASSWORD_ENCODER.encode("librarian1"));
            user.setRole(roleRepository.findByName("ROLE_LIBRARIAN"));
            user.setEnabled(true);
            user.setBarcode("5555555");
            user.setZipCode("1091");
            user.setCity("Budapest");
            user.setStreetAddress("Próba utca 70/A");
            userRepository.save(user);
        }

    }


    public void fillGenreTable() {
        if (genreRepository.findByName("Science Fiction") == null) {
            Genre genre = new Genre("Science Fiction");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Drama") == null) {
            Genre genre = new Genre("Drama");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Satire") == null) {
            Genre genre = new Genre("Satire");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Adventure") == null) {
            Genre genre = new Genre("Adventure");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Romance") == null) {
            Genre genre = new Genre("Romance");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Mystery") == null) {
            Genre genre = new Genre("Mystery");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Horror") == null) {
            Genre genre = new Genre("Horror");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Self help") == null) {
            Genre genre = new Genre("Self help");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Travel") == null) {
            Genre genre = new Genre("Travel");
            genreRepository.save(genre);
        }

        if (genreRepository.findByName("Fantasy") == null) {
            Genre genre = new Genre("Fantasy");
            genreRepository.save(genre);
        }

    }


    public void fillBibliographicRecordTable() {
        // EXAMPLE BIBLIOGRAPHIC RECORD
        if (bibliographicRecordRepository.findAll().isEmpty()) {
            Set<BibliographicRecord> bibliographicRecords = new HashSet<>();
            bibliographicRecords.add(new BibliographicRecord("Egri csillagok", "Gárdonyi Géza", "9783161484101", genreRepository.findByName("Drama")));
            bibliographicRecords.add(new BibliographicRecord("Kinizsi Pál", "Tatay Sandor", "9789635397334", genreRepository.findByName("Adventure")));
            bibliographicRecords.add(new BibliographicRecord("A Pál utcai fiúk", "Molnár Ferenc", "9789631191714", genreRepository.findByName("Drama")));
            bibliographicRecords.add(new BibliographicRecord("Eragon", "Christopher Paolini", "9789630794329", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Alien vs Predator - Armageddon", "Tim Lebbon", "9789630794786", genreRepository.findByName("Horror")));
            bibliographicRecords.add(new BibliographicRecord("Az ember tragédiája", "Madách Imre", "9789633896235", genreRepository.findByName("Drama")));
            bibliographicRecords.add(new BibliographicRecord("Narnia 3. - A ló és kis gazdája", "C. S. Lewis", "9789632881423", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Narnia 4. - Caspian herceg", "C. S. Lewis", "9789632881430", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Brisingr", "Christopher Paolini", "9789630796316", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Elsőszülött", "Christopher Paolini", "9789630794336", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Harry Potter és a Bölcsek Köve", "J. K. Rowling", "9789633244531", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Harry Potter és a titkok kamrája", "J. K. Rowling", "9789633244814", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Harry Potter és az azkabani fogoly", "J. K. Rowling", "9789633245033", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("Harry Potter és a Tűz Serlege", "J. K. Rowling", "9789639884762", genreRepository.findByName("Fantasy")));
            bibliographicRecords.add(new BibliographicRecord("A pozitív gondolkodás ereje", "Dr. Norman Vincent Peale", "9780743234801", genreRepository.findByName("Self help")));
            bibliographicRecords.add(new BibliographicRecord("Az", "Stephen King", "9789634058045", genreRepository.findByName("Horror")));
            bibliographicRecords.add(new BibliographicRecord("Toszkána - Utazik a család", "Zsiga Henrik", "5999884910046", genreRepository.findByName("Travel")));
            bibliographicRecords.add(new BibliographicRecord("Bioszféra", "Robert Charles Wilson", "9789639866447", genreRepository.findByName("Science Fiction")));
            bibliographicRecords.add(new BibliographicRecord("Misztérium", "Robert Charles Wilson", "9786155158773", genreRepository.findByName("Science Fiction")));
            bibliographicRecords.add(new BibliographicRecord("Darwinia", "Robert Charles Wilson", "9786155508325", genreRepository.findByName("Science Fiction")));
            bibliographicRecords.add(new BibliographicRecord("Daemon", "Daniel Suarez", "9789632453125", genreRepository.findByName("Science Fiction")));
            bibliographicRecordRepository.save(bibliographicRecords);
        }

    }

    public void fillBookTable() {
        // EXAMPLE BOOK
        if (bookRepository.findByBarcode("1111111") == null) {
            Book book = new Book("1111111",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9783161484101"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111112") == null) {
            Book book = new Book("1111112",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9783161484101"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("2222222") == null) {
            Book book = new Book("2222222",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789635397334"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("3333333") == null) {
            Book book = new Book("3333333",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789631191714"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("4444444") == null) {
            Book book = new Book("4444444",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789630794329"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("5555555") == null) {
            Book book = new Book("5555555",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789630794336"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("6666666") == null) {
            Book book = new Book("6666666",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789633244814"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("7777777") == null) {
            Book book = new Book("7777777",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789633245033"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("8888888") == null) {
            Book book = new Book("8888888",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789639884762"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("8888888") == null) {
            Book book = new Book("8888888",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789639884762"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("9999999") == null) {
            Book book = new Book("9999999",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789630794786"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111112") == null) {
            Book book = new Book("1111112",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789633245033"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111113") == null) {
            Book book = new Book("1111113",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789630794336"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111114") == null) {
            Book book = new Book("1111114",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789632453125"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111115") == null) {
            Book book = new Book("1111115",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789632453125"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111116") == null) {
            Book book = new Book("1111116",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9786155158773"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111117") == null) {
            Book book = new Book("1111117",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("5999884910046"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111118") == null) {
            Book book = new Book("1111118",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789634058045"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111119") == null) {
            Book book = new Book("1111119",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789634058045"));
            bookRepository.save(book);
        }
        if (bookRepository.findByBarcode("1111120") == null) {
            Book book = new Book("1111120",true);
            book.setBibliographicRecord(bibliographicRecordRepository.findByIsbn("9789630794336"));
            bookRepository.save(book);
        }

    }

    public void printSettings() {
        System.out.println(LibraryConfiguration.getBookLoanPeriodInDay());
        System.out.println(LibraryConfiguration.getOverdueFeePerDay());
        System.out.println(environment.getProperty("DB_ADDRESS"));
        System.out.println(environment.getProperty("DB_SCHEMA"));

    }

}
