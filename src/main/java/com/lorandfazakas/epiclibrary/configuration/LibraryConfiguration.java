package com.lorandfazakas.epiclibrary.configuration;


public class LibraryConfiguration {

    private static long bookLoanPeriodInDay = 90;
    private static long overdueFeePerDay = 50;
    private static int membershipExtendInDays = 365;

    public static long getBookLoanPeriodInDay() {
        return bookLoanPeriodInDay;
    }

    public static long getOverdueFeePerDay() {
        return overdueFeePerDay;
    }

    public static void setBookLoanPeriodInDay(long bookLoanPeriodInDay) {
        LibraryConfiguration.bookLoanPeriodInDay = bookLoanPeriodInDay;
    }

    public static void setOverdueFeePerDay(long overdueFeePerDay) {
        LibraryConfiguration.overdueFeePerDay = overdueFeePerDay;
    }

    public static int getMembershipExtendInDays() {
        return membershipExtendInDays;
    }

    public static void setMembershipExtendInDays(int membershipExtendInDays) {
        LibraryConfiguration.membershipExtendInDays = membershipExtendInDays;
    }
}
