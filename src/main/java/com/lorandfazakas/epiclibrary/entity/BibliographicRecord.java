package com.lorandfazakas.epiclibrary.entity;

import java.util.Set;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;

@Entity
public class BibliographicRecord extends BaseEntity{
    @Size(min = 1, max = 80, message = "Title must be between 1 and 80 character long")
    private String title;

    @Size(min = 2, max = 80, message = "Author must be between 2 and 80 character long")
    private String author;

    @Valid
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "genre_id")
    private Genre genre;

    @Column(unique = true)
    @Size(min = 10, max = 17, message = "ISBN must be between 10 and 17 character long")
    private String isbn;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "bibliographicRecord")
    private Set<Book> books;

    public BibliographicRecord() {
        super();
    }

    public BibliographicRecord(String title, String author, String isbn, Genre genre) {
        this();
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
