package com.lorandfazakas.epiclibrary.entity;

import java.time.LocalDate;

import javax.persistence.Entity;

@Entity
public class LoanHistory extends BaseEntity {

    private Long bookId;
    private Long userId;
    private LocalDate loanDate;
    private LocalDate returnDate;

    public LoanHistory() {
        super();
    }

    public LoanHistory(Loan loan) {
        this();
        this.bookId = loan.getBook().getId();
        this.userId = loan.getUser().getId();
        this.loanDate = loan.getLoanDate();
        this.returnDate = loan.getReturnDate();
    }

}
