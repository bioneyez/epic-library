package com.lorandfazakas.epiclibrary.entity;

import java.time.LocalDate;
import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.Size;

import com.lorandfazakas.epiclibrary.configuration.LibraryConfiguration;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User extends BaseEntity implements UserDetails{
    @Size(min = 4, max = 100, message = "Full name must be between 4 and 100 character long")
    private String name;
    @Size(min = 4, max = 16, message = "Zip code must be between 4 and 16 character long")
    private String zipCode;
    @Size(min = 2, max = 35, message = "City name must be between 2 and 35 character long")
    private String city;
    @JsonIgnore
    @Email
    private String email;
    @Size(min = 5, max = 95, message = "Street address must be between 5 and 95 character long")
    private String streetAddress;
    @Column(unique = true)
    @Size(min = 4, max = 15, message = "Username must be between 4 and 15 character long")
    private String username;
    @JsonIgnore
    @Size(min = 4, max = 64, message = "Password must be between 4 and 64 character long")
    private String password;
    @Column(unique = true)
    @Size(min = 7 , message = "Barcode must be exactly 7 character long")
    private String barcode;
    private boolean enabled;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    @JsonIgnore
    private Role role;
    private LocalDate membershipExpiration;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<Loan> loans;

    public User() {
        super();
        loans = new HashSet<>();
        this.membershipExpiration = LocalDate.now().plusDays(LibraryConfiguration.getMembershipExtendInDays());
    }

    public User(String name, String username, String email, String password, Role role, boolean enabled) {
        this();
        this.name = name;
        this.username = username;
        this.email = email;
        setPassword(password);
        this.role = role;
        this.enabled = enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role.getName()));
        return authorities;
    }

    public void newLoan(Loan loan) {
        loans.add(loan);
    }

    public void removeLoan(Loan loan) {
        Iterator<Loan> iter = loans.iterator();
        while (iter.hasNext()) {
            Loan currentLoan = iter.next();
            if(currentLoan.getBook().getBarcode().equals(loan.getBook().getBarcode())) {
                iter.remove();
            }
        }
    }


    public Set<Loan> getLoans() {
        return loans;
    }

    public void setLoanedBooks(Set<Loan> loans) {
        this.loans = loans;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public void setLoans(Set<Loan> loans) {
        this.loans = loans;
    }

    public LocalDate getMembershipExpiration() {
        return membershipExpiration;
    }

    public void setMembershipExpiration(LocalDate membershipExpiration) {
        this.membershipExpiration = membershipExpiration;
    }
}
