package com.lorandfazakas.epiclibrary.entity;

import javax.persistence.Entity;

import com.lorandfazakas.epiclibrary.enums.RoleEnum;

@Entity
public class Role extends BaseEntity{
    private String name;

    public Role() {
        super();
    }


    public Role(RoleEnum roleEnum) {
        this();
        this.name = roleEnum.name();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
