package com.lorandfazakas.epiclibrary.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Book extends BaseEntity{

    @Column(unique = true)
    @Size(min = 7, max = 7, message = "The field must be exactly 7 character long")
    private String barcode;
    private boolean available;

    @ManyToOne
    @JoinColumn(name = "bibliographic_record_id")
    private BibliographicRecord bibliographicRecord;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "book")
    private Loan loan;

    public Book() {
        super();
    }

    public Book(String barcode, boolean available) {
        this.barcode = barcode;
        this.available = available;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public BibliographicRecord getBibliographicRecord() {
        return bibliographicRecord;
    }

    public void setBibliographicRecord(BibliographicRecord bibliographicRecord) {
        this.bibliographicRecord = bibliographicRecord;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
}
