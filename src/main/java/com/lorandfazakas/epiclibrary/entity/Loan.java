package com.lorandfazakas.epiclibrary.entity;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.lorandfazakas.epiclibrary.configuration.LibraryConfiguration;
import org.springframework.beans.factory.annotation.Autowired;

@Entity
public class Loan extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private LocalDate loanDate;
    private LocalDate returnDateDeadline;
    private LocalDate returnDate;

    public Loan() {
        super();
    }

    public Loan(User user, Book book) {
        this();
        this.user = user;
        this.book = book;
        this.loanDate = LocalDate.now();
        this.returnDateDeadline = loanDate.plusDays(LibraryConfiguration.getBookLoanPeriodInDay());
    }

    public long calculateOverdueFee() {
        long overdueFee = 0;
        long daysBetweenLoanAndToday = returnDateDeadline.until(LocalDate.now(), ChronoUnit.DAYS);
        if (daysBetweenLoanAndToday > 0) {
            long overdueDayCount = daysBetweenLoanAndToday;
            overdueFee = overdueDayCount * LibraryConfiguration.getOverdueFeePerDay();
        }

        return overdueFee;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(LocalDate loanDate) {
        this.loanDate = loanDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public LocalDate getReturnDateDeadline() {
        return returnDateDeadline;
    }

    public void setReturnDateDeadline(LocalDate returnDateDeadline) {
        this.returnDateDeadline = returnDateDeadline;
    }
}
