package com.lorandfazakas.epiclibrary.entity;

import javax.persistence.Entity;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
public class Genre extends BaseEntity {

    @Size(min = 1, max = 30, message = "Genre name must be between 1 and 30 character long")
    private String name;

    public Genre() {
        super();
    }

    public Genre(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(name, genre.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }
}
