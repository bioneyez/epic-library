$(function(){
    // Activate mobile nav toggle button
    $(".button-collapse").sideNav({edge: 'right'});

    // Activate close icons for dismissible alerts
    $('[data-close]').on('click',function(){
        $(this).parent().remove();
    });

    $('#searchIcon').on('click',function(){
        $("#searchForm").submit();
    });

});